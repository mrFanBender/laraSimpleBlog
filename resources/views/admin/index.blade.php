@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="jambotron">
                    <p>
                        <span class="label label-primary">
                            Категории
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="jambotron">
                    <p>
                        <span class="label label-primary">
                            Материалы
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="jambotron">
                    <p>
                        <span class="label label-primary">
                            Посетителей
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="jambotron">
                    <p>
                        <span class="label label-primary">
                            Посетителей сегодня
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <a href="#" class="btn btn-primary">Создать</a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">
                        Категория 1
                    </h4>
                    <p class="list-group-item-text">
                        кол-во материалов
                    </p>
                </a>
            </div>
            <div class="col-sm-6">
                <a href="#" class="btn btn-primary">Создать</a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">
                        Материал
                    </h4>
                    <p class="list-group-item-text">
                        Категория
                    </p>
                </a>
            </div>
        </div>
    </div>
@endsection()
