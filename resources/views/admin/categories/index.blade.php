@extends('admin.layouts.app')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumbs')
            @slot('title') Список категорий @endslot
            @slot('parent') Главная @endslot
            @slot('active') Категории @endslot
        @endcomponent()
        <a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right">Создать категорию</a>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Наименование</th>
                <th scope="col">Активна</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($categories as $category)
                <tr>
                    <th scope="row">{{$category->id}}</th>
                    <td>{{$category->title}}</td>
                    <td>{{$category->published}}</td>
                    <td>
                        <a href="{{route('admin.category.edit', ['id'=>$category->id])}}"><i class="fa fa-edit"></i></a>
                        <a href="{{route('admin.category.destroy', ['id'=>$category->id])}}"><i class="fa fa-remove"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">Категории отсутсвуют</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4">
                            <ul class="pagination">
                                {{$categories->links()}}
                            </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection()